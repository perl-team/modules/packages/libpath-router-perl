libpath-router-perl (0.15-3) unstable; urgency=medium

  * Team upload.
  * Remove Makefile.old via debian/clean. (Closes: #1045558)

 -- gregor herrmann <gregoa@debian.org>  Thu, 07 Mar 2024 17:01:51 +0100

libpath-router-perl (0.15-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.
  * debian/*: update GitHub URLs to use HTTPS.

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from deprecated 9 to 10.
  * Remove 1 unused lintian overrides.
  * Set Testsuite header for perl package.
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on debhelper, libclone-pp-perl,
      perl, libeval-closure-perl, perl, libmoo-perl, perl, perl,
      libtest-simple-perl, libtest-deep-perl, libtry-tiny-perl,
      libtype-tiny-perl, perl, libnamespace-clean-perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 16 Jun 2022 23:49:58 +0100

libpath-router-perl (0.15-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 15:49:25 +0100

libpath-router-perl (0.15-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#817270.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 09 Mar 2016 18:22:53 +0100
